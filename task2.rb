def same_names
	names = ['Ahmad', 'Tono', 'Tini', 'Bambang', 'Agus', 'Agung']
	result = []
	names.each do |name|
		if name.length % 2 == 1 && name.split.map(&:chr).join == "A"
			res = name.upcase
		elsif name.split.map(&:chr).join == "T"
			res = name.downcase
		else
			res = name.reverse.capitalize
		end
		result << res
	end
	return result
end

final = same_names
puts "names = #{final}"