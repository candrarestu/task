#pajak dikenakan jika pendapatan lebih besar daripada pengeluaran
#pajak senilai n% dari laba kotor (pendapatan - pengeluaran)
def tax_calc(revenue, cost, tax_percent)
	if revenue > cost
		tax = (revenue - cost) * tax_percent / 100
	else
		tax = 0
	end
end

#besarnya net income senilai pendapatan - pengeluaran - pajak
def net_income_calc(revenue, cost, tax_percent)
	net_incom = (revenue - cost) - tax_calc(revenue, cost, tax_percent)
end

#besarnya margin senilai rasio net income dibandingkan pendapatan
#desimal margin dibulatkan dalam 2 digit
#margin dinyatakan dalam persen
def margin_calc(revenue, cost, tax_percent)
	margin = (net_income_calc(revenue, cost, tax_percent) / revenue) * 100
	margin.round(2)		#return hasil margin menjadi pembulatan 2 kebelakang
end

puts "Input total pendapatan: "
revenue = gets.chomp.to_f
puts "Input total biaya: "
cost = gets.chomp.to_f
puts "Input pajak (dalam persen): "
tax_percent = gets.chomp.to_f

puts "Total pendapatan: #{revenue}"
puts "Total pengeluaran: #{cost}"
puts "Total pajak: #{tax_calc(revenue, cost, tax_percent)}"
puts "Keuntungan: #{net_income_calc(revenue, cost, tax_percent)}"
puts "Margin(dalam persen): #{margin_calc(revenue, cost, tax_percent)} %"