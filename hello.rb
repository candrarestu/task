#hello world example
puts "My name is candra"

#tipe data angka
#Hitung angka
angka1 = 25		##integer
angka2 = 26.5	##float

puts "Hasil Penjumlahan #{angka1 + angka2}"

#tipe data string
nama1 = "Fred"
puts "namanya adalah #{nama1}"


#tipe data boolean (kondisi antara true dan false)
bool1 = false
if bool1 == true
	puts "TRUE"
else
	puts "FALSE"
end


# tipe data symbol
simbolku = {name: "Fred", occupation: "Kebumen"}

puts simbolku[:name]
puts simbolku[:occupation]

#tipe data hash
hashku = {:kota => "Magelang"}
puts hashku


#tipe data array
nomor1 = [1,2,3,4,5,6]


#foreach loop
nomor1.each do |angkanya|
	if angkanya == 5
		puts "Nemu angka 5nya bro: #{angkanya}"
	else 
		puts "Ndak ketemu bro angka : #{angkanya}"
	end
end
